# STI Technical Evaluation Assignment Response

The STI RESTful API application was built using the [JHipster](https://jhipster.github.io/documentation-archive/v4.1.1) code generator.

If you are importing the application into Eclipse, I recommend [Neon](http://www.eclipse.org/downloads/packages/release/Neon/3).  After importing the code using _Import -> Projects from Git_), simply right click on the project root and select _Maven -> Update Project_ to fix any compilation issues.

## Launching the application

### Prerequisites

To launch the application, install Docker and Docker Compose:

- [Docker](https://docs.docker.com/installation/#installation) (version 17.03.1-ce)
- [Docker Compose](https://docs.docker.com/compose/install/) (version 1.11.2).  

Docker will take care of all other dependencies.

### Start the container

To launch the application, **_ensure that Docker is running_** and execute the following commands from the project root folder:

```
   ./mvnw package docker:build
  docker-compose -f src/main/docker/app.yml up
```

You should then be able to load the application in a browser at http://localhost:8080/ and see the following:

![Home Page](src/main/webapp/content/images/help_home.png "Home Page")


### Default Accounts

 You can login using any of the default accounts:
- Administrator (login="admin" and password="admin")
- User (login="user" and password="user").

## Application architecture
JHipster is a Yeoman generator that leverages Spring Boot and Angular, so the application is Spring based and follows common Spring conventions.  For simplicity, I chose a "monolithic" architecture rather than a microservice architecture, meaning the server side Spring Boot code and the front-end AngularJS code are bundled together.
 
- The application is [dockerized](https://www.docker.com/)
- Maven is used for building, testing and running the application
- Application configuration is handled by [Spring Boot](http://projects.spring.io/spring-boot/) with environment specific configuration externalized using YAML files
- User authentication is performed by [Spring Security](http://projects.spring.io/spring-security/) and JSON Web Tokens ([JWT](https://jwt.io/introduction/))
- Database access is managed by [Spring Data JPA](http://projects.spring.io/spring-data-jpa/) and [Hibernate](http://hibernate.org/), with second level caching enabed using [ehcache](http://www.ehcache.org/)
- [PostgreSQL](https://www.postgresql.org/) is used for the database
- The REST API is implemented using [Spring MVC Rest](https://spring.io/guides/gs/rest-service/) and [Jackson](https://github.com/FasterXML/jackson)
- The front end uses [Angular 1.x](https://angularjs.org/), is responsive and internationalized

## The Foo API

### Code Layering

For the purpose of this exercise, a [Foo](src/main/java/ca/roundwheel/sti/domain/Foo.java) JPA entity was created with two parameters, _bar_ and _baz_, as well as the _id_ primary key.  The REST API for creating and modifying _Foo_ data is defined in the Spring RestController [FooResource](https://github.com/roundwheel/sti-jwt/blob/master/src/main/java/ca/roundwheel/sti/web/rest/FooResource.java).  @PostMapping, @PutMapping, @GetMapping, and @DeleteMapping annotations are used to define each of the URL mappings that the REST API supports.  

_FooResource_ interacts with the back end via the [FooService](src/main/java/ca/roundwheel/sti/service/FooService.java) facade, which in turn persists or deletes _Foo_ entities using the [FooRepository](https://github.com/roundwheel/sti-jwt/blob/master/src/main/java/ca/roundwheel/sti/repository/FooRepository.java), which is itself a [Spring Data JPA Repository](http://docs.spring.io/spring-data/jpa/docs/1.11.1.RELEASE/reference/html/#repositories).  

![Foo Sequence](src/main/webapp/content/images/help-sequence-save.png "Foo Sequence")

### XML Support

Nearly all code and configuration in this application was generated automatically by JHipster.  The custom class [JsonAndXmlSupportConfiguration](https://github.com/roundwheel/sti-jwt/blob/master/src/main/java/ca/roundwheel/sti/config/JsonAndXmlSupportConfiguration.java) was required to enable support for XML request parameters and responses, since out of the box JHipster only supports JSON.  Minimal changes were also required to the [FooResource](https://github.com/roundwheel/sti-jwt/blob/master/src/main/java/ca/roundwheel/sti/web/rest/FooResource.java) RestController to enable XML support. Where applicable, the operation annotation was changed from something like this: 

```java
@PostMapping("/foos")
```
... to this:

```java
@PostMapping(
	value="/foos", 
	consumes={MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
	produces={MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
)
```

Spring automatically handles marshalling of _Foo_ entities to XML or JSON based on the standard approach of specifying the desired format as an **_Accept_** HTTP header when calling the REST API.  Similarly, Spring will automatically unmarshall an XML or JSON string into a _Foo_ entity based on the value of the **_Content-Type_** request header. [See the sample API calls below](https://github.com/roundwheel/sti-jwt#test-using-curl) for examples how to specify the request headers. 

### Enhanced error handling

Although the user interface that JHipster generates does a very good job of preventing users from entering invalid data by giving descriptive error messages, the REST API itself was enhanced slightly to give more meaningful messages when invalid data is submitted (using curl for example).  To do this, _message_ keys were added to the validation annotations in the [Foo entity](https://github.com/roundwheel/sti-jwt/blob/master/src/main/java/ca/roundwheel/sti/domain/Foo.java):

```java
	@NotNull(message = "error.validation.foo.baz.notnull")
	@Min(value = 100, message = "error.validation.foo.baz.min")
	@Max(value = 999, message = "error.validation.foo.baz.max")
	@Column(name = "baz", nullable = false)
	private Integer baz;
```

... key/value pairs were added to the [language bundle](src/main/resources/i18n/messages.properties):

```
# REST Api / Entity validation error messages
error.validation.foo.bar.notnull=A foo needs a bar
error.validation.foo.baz.notnull=A foo needs a baz
error.validation.foo.baz.min=Your baz is too small
error.validation.foo.baz.max=Your baz is too large
```

... and the JHipster provided [@ControllerAdvice implementation](src/main/java/ca/roundwheel/sti/web/rest/errors/ExceptionTranslator.java) was slightly modified to read the error messages from the language bundle:

```java
@Autowired
private MessageSource messageSource;	// added

...

@ExceptionHandler(MethodArgumentNotValidException.class)
@ResponseStatus(HttpStatus.BAD_REQUEST)
@ResponseBody
public ErrorVM processValidationError(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult();
    Locale locale = LocaleContextHolder.getLocale();	// added
    List<FieldError> fieldErrors = result.getFieldErrors();
    ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);
    for (FieldError fieldError : fieldErrors) {
    	String error = messageSource.getMessage(fieldError.getDefaultMessage(), null, locale); // added
        dto.add(fieldError.getObjectName(), fieldError.getField(), error != null ? error : fieldError.getCode());
    }
    return dto;
}
```

## Database Design

Because I chose to use JHipster, the database tables were automatically created.  JHipster would also handle database schema changes automatically using [Liquibase](http://www.liquibase.org/) if modifications were made to the _Foo_ entity.

To browse the database, determine the **CONTAINER ID** of the PostgreSQL container by issuing the following command:

```
docker ps -a
```

The output should look something like this:

<pre>
CONTAINER ID        IMAGE               COMMAND                  CREATED         ...
f0f2eebb6010        postgres:9.5.4      "/docker-entrypoin..."   20 minutes ago  ...
509fc6f1dda2        stijwt              "/bin/sh -c 'echo ..."   20 minutes ago  ...
</pre>

Note the value of the postgres:9.5.4 **CONTAINER ID** and substitute it in the first of the following commands:

```
docker exec -it f0f2eebb6010 bash
psql -U stijwt
```

You are now connected to the database so you can display all the tables used by the application by running:

```
stijwt=# \dt
```

<pre>
                    List of relations
 Schema |             Name              | Type  | Owner  
--------+-------------------------------+-------+--------
 public | databasechangelog             | table | stijwt
 public | databasechangeloglock         | table | stijwt
 public | foo                           | table | stijwt
 public | jhi_authority                 | table | stijwt
 public | jhi_persistent_audit_event    | table | stijwt
 public | jhi_persistent_audit_evt_data | table | stijwt
 public | jhi_social_user_connection    | table | stijwt
 public | jhi_user                      | table | stijwt
 public | jhi_user_authority            | table | stijwt
</pre>
 
To see the _foo_ table, run:

```
stijwt=# \d foo
```

<pre>
             Table "public.foo"
 Column |          Type          | Modifiers 
--------+------------------------+-----------
 id     | bigint                 | not null
 bar    | character varying(255) | not null
 baz    | integer                | not null
Indexes:
    "pk_foo" PRIMARY KEY, btree (id)
</pre>

## The REST API

### User authentication

In order to access any of the API endpoints, you must first authenticate using either of the accounts [specified above](#default-accounts) (admin/admin or user/user).  Authentication is implemented using [Spring Security](http://projects.spring.io/spring-security/) and [JSON Web Tokens](https://jwt.io/introduction/) (JWT).  Failure to authenticate or provide a valid token when making API calls will result in a response like the following:

```json
    {
    "timestamp": "2017-04-04T12:43:42.549+0000",
    "status": 401
    ,"error": "Unauthorized"
    ,"message": "Access Denied"
    ,"path": "/api/foos"
    }
```

To authenticate, call the **/api/authenticate** endpoint with a user's credentials.  A valid user will be assigned an **id_token**.  The value of this token should be submitted with all subsequent API requests as an **Authorization: Bearer** request header.  [See below](https://github.com/roundwheel/sti-jwt#authenticate) for examples.

Whether or not a particular mapping should be secured is defined in [SecurityConfiguration's](src/main/java/ca/roundwheel/sti/config/SecurityConfiguration.java) configure() method.

### Test using curl

#### Authenticate

To test the authenticate API, issue a curl command like the following:

```
curl -H "Content-Type: application/json" -X POST -d '{ "username": "user", "password": "user", "rememberMe":true }' http://localhost:8080/api/authenticate
```

This will return a response like the following:

```json
{
"id_token":"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA"
}
```

The value of the *id_token* must be passed as an **Authorization: Bearer** request header in all subsequent API calls.

#### Add a Foo

##### Using JSON

```
curl -H "Content-Type: application/json" --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' -X POST -d '{ "bar": "this is the test using the JSON API", "baz": 112 }' http://localhost:8080/api/foos
```

##### Using XML

```
curl -X POST --header 'Content-Type: application/xml' --header 'Accept: application/xml' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' -d '<?xml version="1.0"?><Foo><bar>This is a test of adding a FOO using XML</bar> <baz>333</baz></Foo>' 'http://localhost:8080/api/foos'
 ```

##### Field level validation

Note that a _Foo_ must have a _bar_ at least 8 characters long and a _baz_ between 100 and 999.  To see validation errors when these values are not met, run the following:

```
curl -H "Content-Type: application/json" --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' -X POST -d '{ "bar": "short", "baz": 10 }' http://localhost:8080/api/foos
```
This should return:

```json
{
  "message" : "error.validation",
  "description" : null,
  "fieldErrors" : [ {
    "objectName" : "foo",
    "field" : "baz",
    "message" : "Your baz is too small"
  }, {
    "objectName" : "foo",
    "field" : "bar",
    "message" : "A bar must be at least 8 characters long"
  } ]
```  
  
#### Get a list of all Foo

##### Returned as JSON

```
curl -X GET --header 'Accept: application/json' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' 'http://localhost:8080/api/foos/'
```

##### Returned as XML

```
curl -X GET --header 'Accept: application/xml' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' 'http://localhost:8080/api/foos'
```

#### Get a specific Foo

##### Returned as JSON

```
curl -X GET --header 'Accept: application/json' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' 'http://localhost:8080/api/foos/1001'
```

##### Returned as XML

```
curl -X GET --header 'Accept: application/xml' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' 'http://localhost:8080/api/foos/1001'
```

#### Update a Foo

##### Using JSON

```
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' -d '{ "id": 1002, "bar": "this is the modified test using the API", "baz": 222 }' 'http://localhost:8080/api/foos'
```

##### Using XML

```
curl -X PUT --header 'Content-Type: application/xml' --header 'Accept: application/xml' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' -d '<?xml version="1.0"?><Foo><bar>This is updating a FOO 951 XML</bar><baz>444</baz><id>1002</id></Foo>' 'http://localhost:8080/api/foos'
 ```

#### Delete a Foo

```
curl -X DELETE --header 'Accept: */*' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ5MzkwMTU2MH0._aR4pko9mnek84U_NoDqaoylNoCxNuPLHExklP3ZqTEq8woIISKr8MpHmE8azkivq6Pjb0-nN9dzaiffN9wPLA' 'http://localhost:8080/api/foos/1002'
```

### Test using the user interface

If you login to the user interface and go to [Entities > Foo](http://localhost:8080/#/foo), you can see the REST API in action in a traditional web app.  

![CRUD Screen](src/main/webapp/content/images/help-ui.png "CRUD Screen")

All typical create, read, update and delete operations are available using the UI.

### Test using Swagger

All of the _FooResource_ operations can also be testing using [Swagger](http://swagger.io/), which is automatically bundled with a JHipster application.  This tool can be accessed by **logging in as admin/admin* and navigating to [Administration > API](http://localhost:8080/#/docs).  Click "Show/Hide" or "List Operations" on the "foo-resource" row to see each of the operations.  

![Swagger API](src/main/webapp/content/images/help-swagger.png "Swagger API")

Click each operation protocol to expand further.  Sample parameter data can be provided and submitted using the "Try it out!" button.

![Swagger API Results](src/main/webapp/content/images/help-swagger-try.png "Swagger API Results")
