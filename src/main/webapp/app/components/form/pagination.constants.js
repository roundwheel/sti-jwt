(function() {
    'use strict';

    angular
        .module('stijwtApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
