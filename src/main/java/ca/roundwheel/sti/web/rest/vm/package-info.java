/**
 * View Models used by Spring MVC REST controllers.
 */
package ca.roundwheel.sti.web.rest.vm;
