package ca.roundwheel.sti.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Foo.
 */
@Entity
@Table(name = "foo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Foo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotNull(message = "error.validation.foo.bar.notnull")
	@Size(min = 8, message = "error.validation.foo.bar.size")
	@Column(name = "bar", nullable = false)
	private String bar;

	@NotNull(message = "error.validation.foo.baz.notnull")
	@Min(value = 100, message = "error.validation.foo.baz.min")
	@Max(value = 999, message = "error.validation.foo.baz.max")
	@Column(name = "baz", nullable = false)
	private Integer baz;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBar() {
		return bar;
	}

	public Foo bar(String bar) {
		this.bar = bar;
		return this;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public Integer getBaz() {
		return baz;
	}

	public Foo baz(Integer baz) {
		this.baz = baz;
		return this;
	}

	public void setBaz(Integer baz) {
		this.baz = baz;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Foo foo = (Foo) o;
		if (foo.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, foo.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Foo{" + "id=" + id + ", bar='" + bar + "'" + ", baz='" + baz + "'" + '}';
	}
}
