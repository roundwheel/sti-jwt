package ca.roundwheel.sti.config;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * {@code JsonAndXmlSupportConfiguration} is the only piece of custom code in this entire project. It configures the 
 * Spring MVC REST api to support both JSON and XML request and response payloads.  Out of the box, JHipster only 
 * supports JSON requests and responses.  This class is required to satisfy the exercise requirement that XML must also 
 * be supported.
 * <p/>
 * In addition to this class, changes were made to the {@code FooResource} REST controller as well as it's test class
 * {@code FooResourceIntTest}.
 * 
 * @see https://spring.io/blog/2013/05/11/content-negotiation-using-spring-mvc
 * 
 * @author Yvon Comeau
 */
@Configuration
public class JsonAndXmlSupportConfiguration extends WebMvcConfigurerAdapter implements EnvironmentAware {

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureContentNegotiation(org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer)
	 * 
	 * Here we configure the application to also support XML responses.  The standard way of specifying which format we 
	 * want as a response is to provide the {@code Accept} HTTP header.
	 */
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	    configurer.favorPathExtension(false).
			        favorParameter(false).
			        ignoreAcceptHeader(false).	
			        useJaf(false).
			        defaultContentType(MediaType.APPLICATION_JSON).
			        mediaType("xml", MediaType.APPLICATION_XML).
			        mediaType("json", MediaType.APPLICATION_JSON);
	}

	/* (non-Javadoc)
	 * @see org.springframework.context.EnvironmentAware#setEnvironment(org.springframework.core.env.Environment)
	 */
	@Override
	public void setEnvironment(Environment environment) {
		// unused - simply following the JHipter approach
	}

}